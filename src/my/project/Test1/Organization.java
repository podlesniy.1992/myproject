package my.project.Test1;

public class Organization implements Comparable<Organization>{
    private String name;

    public Organization(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Organization o) {
        return name.toLowerCase().compareTo(o.getName().toLowerCase());
    }
}
