package my.project.Test1;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public List<Organization> generate() {

        List<Organization> organizations = new ArrayList<>();
        organizations.add(new Bank("Банк"));
        organizations.add(new Exchange("Обменник"));
        organizations.add(new NovaPoshta("Новая почта"));
        organizations.add(new Bank("ПУМБ"));
        organizations.add(new Exchange("Монобанк"));
        organizations.add(new Bank("Ощадбанк"));

        return organizations;
    }
}
