package my.project.Test1;

public class NovaPoshta extends Organization implements Sender{
    private float procent = 0.04f;

    public NovaPoshta(String name) {
        super(name);
    }

    public void send(int money) {
        System.out.printf("%s: %.2f%n", getName(), money * (1 - procent));
    }
}
