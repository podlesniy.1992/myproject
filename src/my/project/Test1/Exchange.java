package my.project.Test1;

public class Exchange extends Organization implements Exchenger {
    private float exchangeRates = 27.5f;

    public Exchange(String name) {
        super(name);
    }

    public void change(int money) {
        System.out.printf("%s: %.2f%n", getName(), money / exchangeRates);    }
}
