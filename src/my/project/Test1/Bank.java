package my.project.Test1;

public class Bank extends Organization implements Sender, Exchenger {
    private float procent = 0.05f;
    private float exchangeRates = 27.35f;

    public Bank(String name) {
        super(name);
    }

    public void send(int money) {
        System.out.printf("%s: %.2f%n", getName(), money * (1 - procent));
    }

    public void change(int money) {
        System.out.printf("%s: %.2f%n", getName(), money / exchangeRates);
    }


}
